#ifndef DESSINEWORKER_H
#define DESSINEWORKER_H

#include <QThread>
#include <QObject>
#include <QFile>
#include <QList>
#include <QString>
#include <QDebug>
#include <vector>
#include "orb_interface_i.h"
#include "orb_interface.hh"

class ReceiverThread : public QThread
{
    Q_OBJECT

public:
    ReceiverThread();
    ReceiverThread(int argc, char** argv);

    void run() Q_DECL_OVERRIDE
    {
        startServer();
    }
    void stopServer();

private:
    int argc;
    char** argv;
    CORBA::ORB_var orb;
    void startServer();

    void receiveImageFromORB(QImage&);
    void receiveImageWithCommentsFromORB(QImage&, QList<QString>&);

signals:
    void serverReady() const;
    void serverDestroyed() const;
    void receiveImage(const QImage& img) const;
    void receiveImageWithComments(const QImage& img, const QList<QString>& comments) const;
};

#endif // DESSINEWORKER_H
