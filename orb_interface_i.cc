#include "orb_interface_i.h"
//
// Example implementation code for IDL interface 'bytes_transmission::Dessine'
//
bytes_transmission_Dessine_i::bytes_transmission_Dessine_i(){
  // add extra constructor code here
}
bytes_transmission_Dessine_i::~bytes_transmission_Dessine_i(){
  // add extra destructor code here
}

// Methods corresponding to IDL attributes and operations
::CORBA::Boolean bytes_transmission_Dessine_i::sendImage(const bytes_transmission::Image& img)
{
    QByteArray imgData;

    for(CORBA::ULong i = 0; i < img.bytesCount; i++)
    {
        imgData.append(img.data[i]);
    }

    QImage qimg(img.width, img.height, QImage::Format_Indexed8);
    qimg = QImage::fromData(imgData);

    qDebug() << "Received Image (" << img.width << "x" << img.height << ")";

    receiveImage(qimg);
    return true;
}

::CORBA::Boolean bytes_transmission_Dessine_i::sendImageWithComments(const bytes_transmission::Image& img, const bytes_transmission::StringArray& comments)
{
    QByteArray imgData;

    for(CORBA::ULong i = 0; i < img.bytesCount; i++)
    {
        imgData.append(img.data[i]);
    }

    QImage qimg(img.width, img.height, QImage::Format_Indexed8);
    qimg = QImage::fromData(imgData);

    qDebug() << "Received Image (" << img.width << "x" << img.height << ")";



    QList<QString> cm;
    for(CORBA::ULong i = 0; i < comments.length(); i++)
    {
        cm << QString::fromUtf8(comments[i]);
    }

    receiveImageWithComments(qimg, cm);
    return true;
}
