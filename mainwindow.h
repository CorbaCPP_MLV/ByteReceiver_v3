#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>
#include <QList>
#include "receiverthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_connectPushButton_clicked();

    void on_disconnectPushButton_clicked();

private:
    void onReceiveImageWithComments(const QImage & img, const QList<QString>& comments );

    Ui::MainWindow *ui;
    ReceiverThread *receiverThread;
};

#endif // MAINWINDOW_H
