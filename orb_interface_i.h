#ifndef ORB_INTERFACE_I_H
#define ORB_INTERFACE_I_H

#include <iostream>
#include <QImage>
#include <QDebug>
#include <QString>
#include <QList>
#include <vector>
#include <orb_interface.hh>
#include <boost/signals2.hpp>

using namespace boost::signals2;

class bytes_transmission_Dessine_i : public POA_bytes_transmission::Dessine
{
private:
    // Make sure all instances are built on the heap by making the
    // destructor non-public
    //virtual ~bytes_transmission_Dessine_i();

public:
    // standard constructor
    bytes_transmission_Dessine_i();
    virtual ~bytes_transmission_Dessine_i();

    signal<void (QImage&)> receiveImage;
    signal<void (QImage&, QList<QString>&)> receiveImageWithComments;

    // methods corresponding to defined IDL attributes and operations
    ::CORBA::Boolean sendImage(const bytes_transmission::Image& img);
    ::CORBA::Boolean sendImageWithComments(const bytes_transmission::Image& img, const bytes_transmission::StringArray& comments);
};


#endif // ORB_INTERFACE_I_H
