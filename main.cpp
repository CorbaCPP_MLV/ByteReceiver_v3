#include "mainwindow.h"
#include <QApplication>
#include <QList>
#include <QString>

int main(int argc, char *argv[])
{
    //Register the meta-type for the list of string, since without is not possible to pass a complex type
    //on cross-thread signal
    qRegisterMetaType< QList<QString> >( "QList<QString>" );

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
