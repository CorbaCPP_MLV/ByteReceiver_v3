#include "receiverthread.h"

ReceiverThread::ReceiverThread(int argc, char** argv)
{
    this->argc = argc;
    this->argv = argv;
}

void ReceiverThread::receiveImageFromORB(QImage& img)
{
    emit receiveImage(img);

}

void ReceiverThread::receiveImageWithCommentsFromORB(QImage& img, QList<QString>& cm)
{
    emit receiveImageWithComments(img, cm);
}


void ReceiverThread::stopServer()
{
    orb->destroy();
    emit serverDestroyed();
    quit();
    wait();
}

void ReceiverThread::startServer()
{
    try
    {
        // Initialise the ORB.
        orb = CORBA::ORB_init(argc, argv);

        // Obtain a reference to the root POA.
        CORBA::Object_var obj = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var poa = PortableServer::POA::_narrow(obj);

        // We allocate the objects on the heap.  Since these are reference
        // counted objects, they will be deleted by the POA when they are no
        // longer needed.
        bytes_transmission_Dessine_i* mybytes_transmission_Dessine_i = new bytes_transmission_Dessine_i();

        //Signal connection
        mybytes_transmission_Dessine_i->receiveImage.connect(bind(&ReceiverThread::receiveImageFromORB, this, _1));
        mybytes_transmission_Dessine_i->receiveImageWithComments.connect(bind(&ReceiverThread::receiveImageWithCommentsFromORB, this, _1, _2));

        // Activate the objects.  This tells the POA that the objects are
        // ready to accept requests.
        PortableServer::ObjectId_var mybytes_transmission_Dessine_iid = poa->activate_object(mybytes_transmission_Dessine_i);


        // Obtain a reference to each object and output the stringified
        // IOR to stdout
        {
            // IDL interface: bytes_transmission::Dessine
            CORBA::Object_var ref = mybytes_transmission_Dessine_i->_this();
            CORBA::String_var sior(orb->object_to_string(ref));
            std::cout << "IDL object bytes_transmission::Dessine IOR = '" << (char*)sior << "'" << std::endl;

            //------------------------------------------------------------------------
            QString iorPath("/tmp/ior");
            QFile caFile(iorPath);
            caFile.open(QIODevice::WriteOnly | QIODevice::Text);

            if(!caFile.isOpen())
            {
                qDebug() << "- Error, unable to open" << iorPath << "for output";
            }
            QTextStream outStream(&caFile);
            outStream << sior;
            caFile.close();
        }



        // Obtain a POAManager, and tell the POA to start accepting
        // requests on its objects.
        PortableServer::POAManager_var pman = poa->the_POAManager();
        pman->activate();

        emit serverReady();
        orb->run();
    }
    catch(CORBA::TRANSIENT&)
    {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                  << "server." << std::endl;
    }
    catch(CORBA::SystemException& ex)
    {
        std::cerr << "Caught a CORBA::" << ex._name() << std::endl;
    }
    catch(CORBA::Exception& ex)
    {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
    }
}
