#-------------------------------------------------
#
# Project created by QtCreator 2017-01-15T20:39:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CorbaByteReceiver3
TEMPLATE = app
CONFIG += console c++11 app_bundl
LIBS += -lboost_system -lboost_signals
LIBS += -lomniORB4 -lomnithread -lomniDynamic4


SOURCES += main.cpp\
    orb_interface_i.cc \
    orb_interfaceSK.cc \
    receiverthread.cpp \
    mainwindow.cpp

HEADERS  += \
    orb_interface.hh \
    orb_interface_i.h \
    receiverthread.h \
    mainwindow.h

FORMS    += \
    mainwindow.ui

DISTFILES += \
    orb_interface.idl
